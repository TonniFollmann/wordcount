// Word count that takes special chars and endlines into account when counting, so that
// it will count correctly in case of user input.
// The danish language setting is set to work with the danish keyboard layout on windows 10, possible other windows versions aswell
// but can be easely expanded to support ASCII encoding or other locales aswell.
//
// LICENSE: BSD
// Author: Tonni Follmann.

int isLetter(char); // returns true if char is a-z or A-Z
int isNordicChar(char); // takes a char and returns true if � � � or � � �.
int isEOL(char); // if char = \0 return 1 else return 0
int WordCount(char string[], int size, char language); // returns number of words in a string/char array, language: d for danish , e for english. 