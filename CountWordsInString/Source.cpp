#include <stdio.h>
#include <conio.h>
#include "WordCount.h"
#define stringSize 250

int main()
{
	char stringtoparse[stringSize]; // indeholder s�tning.
	char sprog; // indeholder sprogvalg

	do{
		printf("\nV\x91lg tastatur indstilling, tast D for Dansk, eller tast E for engelsk\n");	
		printf("\n Choose the keyboarrd setting, type D for Danish or E for English\n");//v�lg sprog indstilling.
		sprog = getch();
		switch (sprog){
		case 'e':
		case 'E': // s�tter sprog til engelsk
			printf("Input sentence on a maximum of %d signs.\n", stringSize);
			sprog = 'e';
			break;
		case 'd':
		case 'D':// s�tter sprog til dansk
			printf("\nDu har valgt dansk. Denne er kun testet p� dansk Windows 10,\nda input sproget er Operativsystem specifikt.\n");
			printf("\nIndtast en s\x91tning p\x86 max %d tegn\n", stringSize);
			sprog = 'd';
			break;
		default:  // ukendt sprog, pr�v igen
			printf("\nDet valgte sprog kunne ikke findes\n");
			printf("\nThe chosen language could not be found");
		}
	} while (sprog != 'e' && sprog != 'd'); // pr�v igen ved ukendt sprog.

	fgets(stringtoparse, stringSize, stdin); // henter s�tning fra brugeren.
	
	switch (sprog) {
	case 'e': // engelsk
		printf("\nYou have entered:\n%s\n", stringtoparse);
		printf("\nNumber of words in the sentence entered: %d\n", WordCount(stringtoparse, stringSize, sprog));
		break;
	case 'd': // danish
		printf("\nDu har indtastet: \n %s\n", stringtoparse);
		printf("\nDer er %d ord i indtastningen.\n", WordCount(stringtoparse, stringSize, sprog));
		break;
	}
	return 0;
	// brugt til at finde decimalv�rdier af specialtegn under windows 10: 
	// printf("\nforste bogstav er %c dens decimalv�rdi er %d\n", stringtoparse[0], stringtoparse[0]);
}