#include<stdio.h>
int isLetter(char char1){
	if (char1 > 96 && char1 < 123) { // checker om char1 = en ASCII v�rdi svarende til et bogstav mellem a og z
		return 1;
	}
	else if (char1 > 64 && char1 < 91){ // checker om char1 = en ASCII v�rdi svarende til et bogstav mellem A og Z
		return 1;
	}
	else {
		return 0;
	}
}
int isNordicChar(char char1){
	switch (char1){
	// v�rdier fremkommet ud fra at tage input fra keyboard p� windows 10, da ascii v�rdier ikke passer med inputtet i windows 10.
	case -111: // �
	case -110: // �
	case -122: // �
	case -113: // �
	case -101: // �
	case -99:  // �
		return 1;
		break;
	default: 
		return 0;
	}
}
int isEOL(char char1){
	if (char1 == '\0'){
		return 1;
	}
	else {
		return 0;
	}
}
int WordCount(char string[], int size, char lang){
	int i = 0; // counter
	int numberofwords=0; // variable til at holde antallet af ord.
	switch (lang) { // tester for sprogvalg
		case 'e': // engelsk
			while (i < size && !isEOL(string[i])) {
				for (i; !isLetter(string[i]) && !isEOL(string[i]); i++); // hvis specialtegn mellemrum mv, alt andet end bogstaver. inc i
				for (i; isLetter(string[i]); i++); // inc. i hvis der er tale om et bogstav.
				if (!isEOL(string[i])) numberofwords++; // counter et ord n�r en serie af bogstaver slutter
			}
			return numberofwords; // returner antal ord
			break;
		case 'd': // dansk
			while (i < size && !isEOL(string[i])) {
				for (i; !isLetter(string[i]) && !isNordicChar(string[i]) && !isEOL(string[i]); i++); // hvis specialtegn, men ikke \0 inc i.
				for (i; isLetter(string[i]) || isNordicChar(string[i]); i++); // inc i hvis der er tale om et dansk bogstav.
				if (!isEOL(string[i])) numberofwords++; // counter et ord n�r en serie af bogstaver slutter
			}
			return numberofwords; // returnere antal ord.
			break;
	}
}